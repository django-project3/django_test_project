from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    USER_CHOICES = (
        ("GOLD", "GOLD"),
        ("SILVER", "SILVER"),
        ("BRONZE", "BRONZE"),
    )

    name = models.CharField(max_length=255)
    age = models.IntegerField(default=0)
    user_type = models.CharField(
                                    choices=USER_CHOICES,
                                    max_length=100,
                                    null=True
                                )

    def __str__(self):
        return str(self.username)

    def to_json(self):
        return {
            "name": self.name,
            "age": self.age,
        }

from user.models import CustomUser
from django.http import JsonResponse


def index(request):
    query = CustomUser.objects.all()
    result = [user.to_json() for user in query]
    return JsonResponse(result, safe=False)

import logging
import time

from django.core.cache import cache
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django import http

CACHE_TTL = getattr(settings, "CACHE_TTL", DEFAULT_TIMEOUT)


class LogUserIpMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
        self.logger = logging.getLogger(__name__)

    def __call__(self, request):
        user_ip = request.META.get("REMOTE_ADDR")

        user_data = None

        throttle_time_out = time.time() + 60
        cache_user = cache.get(user_ip)
        auth_check = False

        request_threshold = 0
        if request.user.is_authenticated:
            auth_check = True
            user_data = {
                "user_name": request.user.username,
                "user_type": request.user.user_type
            }
            if user_data["user_type"] == "GOLD":
                request_threshold = 10
            elif user_data["user_type"] == "SILVER":
                request_threshold = 5
            elif user_data["user_type"] == "BRONZE":
                request_threshold = 3
        else:
            request_threshold = 2

        if cache_user:
            request_count = cache_user.get("request_count")
        else:
            request_count = 1

        if cache_user:
            request_time = cache_user.get("throttle_time_out")
            if request_count < request_threshold:
                if time.time() > request_time:
                    request_count = 0
                    request_time = time.time() + 60
                else:

                    request_count += 1
                self.logger.info(f"User IP:{user_ip}")
                if auth_check:
                    cache.set(
                        f"{user_ip}",
                        {
                            "user_id": user_ip,
                            "user_name": user_data["user_name"],
                            "user_type": user_data["user_type"],
                            "request_count": request_count,
                            "throttle_time_out": request_time,
                        },
                        timeout=CACHE_TTL,
                    )
                else:
                    cache.set(
                        f"{user_ip}",
                        {
                            "user_id": user_ip,
                            "request_count": request_count,
                            "throttle_time_out": request_time
                        },
                        timeout=CACHE_TTL,
                    )
                return self.get_response(request)
            else:
                if time.time() > request_time:
                    request_count = 0
                    request_time = time.time()
                    if auth_check:
                        cache.set(
                            f"{user_ip}",
                            {
                                "user_id": user_ip,
                                "user_name": user_data["user_name"],
                                "user_type": user_data["user_type"],
                                "request_count": request_count,
                                "throttle_time_out": request_time,
                            },
                            timeout=CACHE_TTL,
                        )
                    else:
                        cache.set(
                            f"{user_ip}",
                            {
                             "user_id": user_ip,
                             "request_count": request_count,
                             "throttle_time_out": request_time},
                            timeout=CACHE_TTL,
                        )
                return http.HttpResponseForbidden("Request Amount Exceed")
        else:
            self.logger.info(f"User IP:{user_ip}")
            if auth_check:
                cache.set(
                    f"{user_ip}",
                    {
                        "user_id": user_ip,
                        "user_name": user_data["user_name"],
                        "user_type": user_data["user_type"],
                        "request_count": request_count,
                        "throttle_time_out": throttle_time_out,
                    },
                    timeout=CACHE_TTL,
                )
            else:
                cache.set(
                    f"{user_ip}",
                    {
                     "user_id": user_ip,
                     "request_count": request_count,
                     "throttle_time_out": throttle_time_out},
                    timeout=CACHE_TTL,
                )
            return self.get_response(request)

from django.contrib.auth.admin import UserAdmin
from django.contrib import admin
from .form import UserForm
from user.models import CustomUser


class UserAdminClass(UserAdmin):
    add_form = UserForm

    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("username",
                           "name",
                           "user_type",
                           "password1",
                           "password2"
                           ),
            },
        ),
    )


admin.site.register(CustomUser, UserAdminClass)
